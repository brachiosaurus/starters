import os

def count(words: list):
    counts = {}
    for word in words:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1
    total = len(words)
    return counts, total

def most_used(frequencies: dict):
    max_use = max(frequencies.values())
    words = []
    for word in frequencies:
        if frequencies[word] == max_use:
            words.append(word)
    return words, max_use

file = input("enter filepath: ")
while not os.path.isfile(file):
    print("(q) to quit")
    file = input("enter filepath: ")
    if file == 'q':
        break

file = open(file, 'r')
data = file.read()
counts, word_count = count(data.split())
diff_word_count = len(counts.values())
max_used, frequency = most_used(counts)

report = f"""word count: {word_count}
different words: {diff_word_count}
max used word(s): {max_used}
max frequency: {frequency}
"""
print(report)


