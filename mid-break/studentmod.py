
class Student:
    def __init__(self, no, classs):
        self.no = no
        self.classs = classs
        self.grades = {}

    def enter_grade(self, lesson, grades: list):
        if lesson in self.grades:
            if grades[0] != "-":
                self.grades[lesson][0] = grades[0]
            if grades[1] != "-":
                self.grades[lesson][1] = grades[1]
        else:
            self.grades[lesson] = grades


def print_grade(student):
    if not student.grades:
        print("no grade info available")
        return
    lessons = list(student.grades.keys())
    col_width = max([len(x) for x in lessons]) + 4
    print("lesson" + " " * (col_width - 6) + "grade 1   grade 2")
    for lesson in lessons:
        line = lesson + " " * (col_width - len(lesson)) + str(student.grades[lesson][0]) + "        " + str(student.grades[lesson][1])
        print(line)


def print_info(student):
    print("no:", student.no)
    print("class:", student.classs)


students = {}
menu = "--menu--\n(m) menu\n1- add student\n2- enter grade\n3- show student info\n4- delete student\n(q) quit"
if __name__ == '__main__':
    print("student management system")
    print(menu)
    a = True
    while a:
        try:
            match input("--|"):
                case 'm':
                    print(menu)
                case 'q':
                    a = False
                case '1':
                    no = int(input("student no: "))
                    classs = input("class: ")
                    if students and no in [student.no for student in students.values()]:
                        print("this is a duplicate !!")
                        continue
                    student = Student(no, classs)
                    students[no] = student
                case '2':
                    print(list(students.keys()))
                    no = int(input("select student: "))
                    if not students or no not in [student.no for student in students.values()]:
                        print("no matching students")
                        continue
                    lesson = input("enter lesson name: ")
                    index = int(input("1st or 2nd grade?: ")) - 1
                    while index != 0 and index != 1:
                        print("enter 1 or 2")
                        index = int(input("1st or 2nd grade?: ")) - 1
                    grades = ["-", "-"]
                    grade = int(input("enter grade: "))
                    while grade > 100 or grade < 0:
                        print("grade must be in range 0-100")
                        grade = int(input("enter grade: "))
                    grades[index] = grade
                    student = students[no]
                    student.enter_grade(lesson, grades)
                case '3':
                    print(list(students.keys()))
                    no = int(input("select student: "))
                    if not students or no not in [student.no for student in students.values()]:
                        print("no matching students")
                        continue
                    student = students[no]
                    print_info(student)
                    print_grade(student)
                case '4':
                    print(list(students.keys()))
                    no = int(input("select student to delete: "))
                    if not students or no not in [student.no for student in students.values()]:
                        print("no matching students")
                        continue
                    del students[no]
        except TypeError:
            print("enter digits only")











