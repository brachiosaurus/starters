import os
import sys
import time
import shutil

import pwd
import grp
import math


def get_filemodebits(octal_bitmask):
    global filetype_indicator
    filemodebits = ""
    filetypes = {
        '0o100': '-',
        '0o60': 'b',  # blockdev
        '0o20': 'c',  # chardev
        '0o40': 'd',  # directory
        '0o120': 'l',  # symlink
        '0o10': 'p',  # FIFO
        '0o140': 's'  # socket
    }
    if octal_bitmask[:-3] in filetypes:
        filemodebits += filetypes[octal_bitmask[:-3]]

    else:
        filemodebits += "?"
    filetype_indicator = filemodebits

    permissions = {
        '0': "---",
        '1': "--x",
        '2': "-w-",
        '3': "-wx",
        '4': "r--",
        '5': "r-x",
        '6': "rw-",
        '7': "rwx"
    }

    for number in octal_bitmask[-3:]:
        permission = permissions[number]
        filemodebits += permission

    return filemodebits


def user_from_id(userid):
    try:
        user_info = pwd.getpwuid(int(userid))
        return user_info.pw_name
    except KeyError:
        return "?"


def group_from_id(groupid):
    try:
        group_info = grp.getgrgid(int(groupid))
        return group_info.gr_name
    except KeyError:
        return "?"


def format_size(size_in_bytes):
    size_units = ['B', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y']
    size = size_in_bytes
    unit_index = 0

    while size >= 1024 and unit_index < len(size_units) - 1:
        size /= 1024
        unit_index += 1

    formatted_size = f"{size:.1f}{size_units[unit_index]}"
    return formatted_size


def colorize(text):
    color_code = "0"
    bold_code = "0"
    graphic_image_extensions = ["jpeg", "jpg", "gif", "tiff", "psd", "png", "eps", "ai", "indd", "raw"]
    if filetype_indicator == "b" or filetype_indicator == "c":
        color_code = "43;30"
        bold_code = "1"
    elif filetype_indicator == "d":
        color_code = "34"
        bold_code = "1"
    elif filetype_indicator == "l":
        color_code = "36"
        bold_code = "1"
    elif "." in wip_filename:
        base_name, extension = wip_filename.rsplit(".", 1)
        if extension == "exe":
            color_code = "32"
            bold_code = "1"
        elif extension.lower() in graphic_image_extensions:
            color_code = "35"
            bold_code = "1"
    return f"\033[{bold_code};{color_code}m{text}\033[0m"


def ls(directory=".", all_=False, sort="alpha", human=False, size_unit="bytes", colored=True):
    """short for list
    :param directory: path to desired directory
    :param all_: show invisible files if true
    :param sort: sort alphabetically = "alpha", reverse = "reverse", don't sort
    :param human: format size to be human-readable
    :param size_unit: "bytes" or "blocks"
    :return:  a list of dictionaries containing info about files in the given directory
            |filename is the basename
            |filetype is the indicator from
            https://www.gnu.org/software/coreutils/manual/html_node/What-information-is-listed.html
            |permissions are owner's, group's, others' respectively r:read w:write x:execute
            |number of hard links,
            |owner username of owner, if cant be determined: ?
            |group name of owner group, if cant be determined: ?
            |size in bytes without punctuation
            |modification time weeks day, month abbreviated, day, hh:mm:ss, year
    """
    global wip_filename
    global total_in_blocks
    total_in_blocks = 0
    if not os.path.isdir(directory):
        filename = os.path.basename(directory)
        stats = os.stat(directory)
        octal_mode = oct(stats.st_mode)
        size = stats.st_size
        if size_unit == "blocks":
            size = size // 1024
        if human:
            size = format_size(size)
        filemode_bits = get_filemodebits(octal_mode)
        if colored:
            filename = colorize(filename)

        file_info = [{
            "filename": f"{filename}",
            "filemodebits": f"{filemode_bits}",
            "hard links": f"{stats.st_nlink}",
            "owner": f"{user_from_id(stats.st_uid)}",
            "group": f"{group_from_id(stats.st_gid)}",
            "size": f"{size}",
            "mtime": f"{time.ctime(stats.st_mtime)}"
        }]
        total_in_blocks += stats.st_blocks//2
        return file_info

    items = os.listdir(directory)
    if sort == "alpha":
        items.sort(key=lambda item: (item.startswith('.'), item.startswith('_'), item))
    if sort == "reverse":
        items.sort(key=lambda item: (item.startswith('.'), item.startswith('_'), item), reverse=True)
    items.insert(0, "..")
    items.insert(0, ".")

    if not all_:
        items = [item for item in items if not item.startswith('.')]
    files_in_dir = []
    for i in range(0, len(items)):
        filename = items[i]
        wip_filename = filename
        filepath = directory + "/" + filename
        stats = os.stat(filepath)
        octal_mode = oct(stats.st_mode)
        formatted_time = time.strftime("%b %d %H:%M", time.localtime(stats.st_mtime))
        size = stats.st_size
        if size_unit == "blocks":
            size = size // 1024
        if human:
            size = format_size(size)
        filemode_bits = get_filemodebits(octal_mode)
        if colored:
            filename = colorize(filename)
        file_info = {
            "filename": f"{filename}",
            "filemodebits": f"{filemode_bits}",
            "hard links": f"{stats.st_nlink}",
            "owner": f"{user_from_id(stats.st_uid)}",
            "group": f"{group_from_id(stats.st_gid)}",
            "size": f"{size}",
            "mtime": f"{formatted_time}"
        }
        files_in_dir.append(file_info)
        total_in_blocks += stats.st_blocks//2
    return files_in_dir


def format_file_list(file_info_list, filename=True, filemodebits=True, hard_links=True, owner=True,
                     group=True, size=True, mtime=True):
    formatted_strings = []

    max_lengths = {
        "filemodebits": max(len(info["filemodebits"]) for info in file_info_list),
        "hard links": max(len(info["hard links"]) for info in file_info_list),
        "owner": max(len(info["owner"]) for info in file_info_list),
        "group": max(len(info["group"]) for info in file_info_list),
        "size": max(len(info["size"]) for info in file_info_list),
        "mtime": max(len(info["mtime"]) for info in file_info_list)
    }

    for info in file_info_list:
        parts = []

        if filemodebits:
            parts.append(info["filemodebits"].ljust(max_lengths["filemodebits"]))
        if hard_links:
            parts.append(info["hard links"].ljust(max_lengths["hard links"]))
        if owner:
            parts.append(info["owner"].ljust(max_lengths["owner"]))
        if group:
            parts.append(info["group"].ljust(max_lengths["group"]))
        if size:
            parts.append(info["size"].rjust(max_lengths["size"]))
        if mtime:
            parts.append(info["mtime"].ljust(max_lengths["mtime"]))
        if filename:
            parts.append(info["filename"])
        formatted_strings.append(" ".join(parts))

    return "\n".join(formatted_strings)


def generate_columns(filenames):
    """
    :param filenames: a string containing elements separated by \n
    :return: a 2d list of columns containing filenames, each row would fit terminal screen
    """
    file_names = filenames.strip().split('\n')
    file_names = [item.rstrip() for item in file_names]
    terminal_columns = shutil.get_terminal_size().columns

    def find_max_length(strings):
        longest_length = 0
        for string in strings:
            if len(string) > longest_length:
                longest_length = len(string)
        return longest_length + 2

    def split_list_into_sublists(input_list, sublist_count):
        chunk_size = math.ceil(len(input_list) / sublist_count)
        twodlist = [input_list[i:i + chunk_size] for i in range(0, len(input_list), chunk_size)]
        return twodlist

    max_column_width = find_max_length(file_names)
    max_columns = min(len(file_names), terminal_columns // max_column_width, 10)

    max_rows = math.ceil(len(file_names) / max_columns)

    columns = split_list_into_sublists(file_names, max_columns)

    for column in columns:
        while len(column) < max_rows:
            column.append('')

    return columns


def format_and_transpose(twod_list):
    """
    print the rows of given matrix as columns and its columns as rows
    """
    max_lengths = [max(len(item) for item in column) for column in twod_list]
    formatted_matrix = [[item.ljust(max_length + 2) for item in column] for column, max_length in
                        zip(twod_list, max_lengths)]

    transposed_matrix = map(list, zip(*formatted_matrix))
    for row in transposed_matrix:
        print("".join(row))


def throw_error(error, argument):
    if error == "not an option":
        print(f"unrecognized option '{argument}'\nTry '--help' for more information.")
        quit()
    if error == "no such file":
        print(f"ls.py: cannot access '{argument}' : No such file or directory")
        quit()


if __name__ == '__main__':
    if len(sys.argv) == 1:
        list_ = ls()
        columns_ = generate_columns(
            format_file_list(list_, filemodebits=False, hard_links=False, owner=False, group=False, size=False,
                             mtime=False))
        format_and_transpose(columns_)
    else:
        options = ["-a", "--all", "-h", "--human-readable", "--help", "-l", "-r", "--reverse", "-s", "--size", "-U",
                   "--color=always", "--color=never", "--color"]
        args_only = sys.argv
        args_only.pop(0)
        path = "."
        all_ = False
        sort = "alpha"
        human = False
        size_unit = "bytes"
        colored = True

        for argument in args_only:
            if argument not in options:
                if argument.startswith("-"):
                    throw_error("not an option", argument)
                elif not os.path.exists(argument):
                    throw_error("no such file", argument)
                else:
                    path = argument

        help_ = False
        if "--help" in args_only:
            help_ = True
            print("""
Usage: ls [OPTION]... [FILE]...
List information about the FILEs (the current directory by default).
Sort entries alphabetically

Mandatory arguments to long options are mandatory for short options too.
    -a, --all                  do not ignore entries starting with .
        --color[=WHEN]         colorize the output; WHEN can be 'always' (default
                               if omitted), or 'never'; more info below
    -h, --human-readable       with -l and -s, print sizes like 1K 234M 2G etc.
        --help                 show this help
    -l                         use a long listing format
    -r, --reverse              reverse order while sorting
    -s, --size                 print the allocated size of each file, in blocks
    -U                         do not sort; list entries in directory order
            """)
        if "-a" in args_only or "--all" in args_only:
            all_ = True
        if "-r" in args_only or "--reverse" in args_only:
            sort = "reversed"
        if "-U" in args_only:
            sort = "none"
        if "-h" in args_only or "--human-readable" in args_only:
            human = True
        if "-s" in args_only or "--size" in args_only:
            size_unit = "blocks"
        if "--color=alwyas" in args_only:
            colored = True
        if "--color=never" in args_only:
            colored = False
        files = ls(directory=path, all_=all_, sort=sort, human=human, size_unit=size_unit, colored=colored)
        if human:
            total_in_blocks = format_size(total_in_blocks)
        if "-l" in args_only and not help_:
            print("total", total_in_blocks)
            print(format_file_list(files))
        elif size_unit == "blocks" and not help_:
            print("total", total_in_blocks)
            columns_ = generate_columns(
                format_file_list(files, filemodebits=False, hard_links=False, owner=False, group=False,
                                 mtime=False))
            format_and_transpose(columns_)
        elif not help_:
            columns_ = generate_columns(
                format_file_list(files, filemodebits=False, hard_links=False, owner=False, group=False,
                                 size=False, mtime=False))
            format_and_transpose(columns_)
