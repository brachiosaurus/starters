import random
import curses
from curses import wrapper

class Board:
    
    def __init__(self, column: int) -> list:
        self.oneline = ["_" for i in range(column)]

    def update(self, index, char):
        self.oneline[index] = char
        return self.oneline

    def show(self, window):
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_YELLOW)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_GREEN)
        curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_BLACK)

        yellow  = curses.color_pair(1)
        green = curses.color_pair(2)
        black = curses.color_pair(3)


        def check_attribute(char: str, ch_index, answer: str):
            if char == "_":
                return black
            if answer[ch_index] == char:
                return green
            elif char in answer:
                return yellow
            return black

        _, win_columns = window.getmaxyx()
        h_padding = (win_columns - len(self.oneline)) // 2
        
        for i, char in enumerate(self.oneline):
            y = trial - 1
            x = h_padding + 2 * i - 2
            window.addstr(y,  x, char, check_attribute(char, i, answer))
        window.refresh()
        greens = [j for j in  [check_attribute(char, i, answer) for i, char in enumerate(self.oneline)] if j == green]
        if len(greens) == len(self.oneline):
             found = True


def numberdle(stdscr):
    global answer
    global trial
    global found
    found = False
    curses.curs_set(0)
    stdscr.clear()
    trial = 1
    rows, columns = stdscr.getmaxyx()
    header = "numberdle"
    stdscr.addstr(0, (columns - 9) // 2, header, curses.A_UNDERLINE)
    footer = "press (q) to quit"
    stdscr.addstr(rows - 1, (columns - len(footer)) // 2, footer, curses.A_DIM)
    stdscr.refresh()

    board_win = curses.newwin(rows - 3, 25, 2, (columns - 25) // 2)
    board_win.clear()
    board = Board(5)
    numbers = [str(random.randint(0, 9)) for i in range(5)]
    answer = "".join(numbers)
    board.show(board_win)

    while not found:
        key = board_win.getkey() 
        try:
            cursor_on = board.oneline.index("_")
        except ValueError:
            cursor_on = len(board.oneline)
        if key == "q":
            quit()
        elif key == "\n" or cursor_on == len(board.oneline):
            trial += 1
            if rows - 15 < trial:
                break
            board = Board(5)
            board.show(board_win)
            continue
        elif not key.isdigit():
            continue
        else:
            board.update(cursor_on, key) 
            board.show(board_win)       
    
    board_win.addstr(trial + 1, 0, f"the answer: {answer}\ntrials: {trial}")
    board_win.refresh()
    stdscr.refresh()

    while True:
        key = stdscr.getkey() 
        if key == "q":
            break    


if __name__ == '__main__':
    wrapper(numberdle)
