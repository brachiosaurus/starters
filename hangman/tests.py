import curses
from curses import wrapper



def test(stdscr):
    stdscr.clear()
    string = stdscr.getstr().decode(encoding="utf-8")
    stdscr.addstr(5,10, string)
    stdscr.refresh()
    stdscr.getch()

if __name__ == '__main__':
    wrapper(test)
