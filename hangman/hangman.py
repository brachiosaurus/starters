import random
import time
import curses

from curses import wrapper
from threading import Thread, Event

def generate_random_word():
    random_letter_index = random.randint(0,28)
    letters = ["A","B","C","Ç","D","E","F","G","H","I","İ","J","K","L","M","N","O","Ö","P","R","S","Ş","T","U","Ü","V","Y","Z"]
    first_letter = letters[random_letter_index]
    with open(f"words/{first_letter}.txt", "r") as file:
        all_lines = file.read().split("\n")
        line_count = len(all_lines)
        random_word_index = random.randint(0,(line_count-1))
        return all_lines[random_word_index]



def add_centered_str(x, str_, stdscr, attribute=None):
    _, columns = stdscr.getmaxyx()
    if attribute:
        stdscr.addstr(x, (columns - len(str_)) // 2, str_, attribute)
    stdscr.addstr(x,(columns - len(str_)) // 2, str_)


class Man:
    pole = """
+-----+
|     
|    
|    
|
--
"""
    head = """
+-----+
|     O
|    
|    
|
--
"""
    torso = """
+-----+
|     O
|     |
|    
|
--
"""

    left_arm = """
+-----+
|     O
|    /|
|    
|
--
"""

    right_arm = """
+-----+
|     O 
|    /|\\
|    
|
--
"""

    left_leg = """
+-----+
|     O
|    /|\\
|    /
|
--
"""

    right_leg = """
+-----+
|     O 
|    /|\\
|    /'\\
|
--
"""
    all_forms = [pole, head, torso, left_arm, right_arm, left_leg, right_leg]


class Table:
    def __init__(self):
        self.b_list = []

    def generate_board(self, word):
        self.b_list[:] = ["_" if i != " " else "  " for i in word]
        self.board = " ".join(self.b_list)

    def refresh_board(self, letter, word):
        i = 0
        for char in word:
            if char == letter:
                if letter == "i":
                    self.b_list[i] = "İ"
                else:
                    self.b_list[i] = letter.upper()
            i += 1
        self.board = " ".join(self.b_list)
        

times_up = Event()
timer_stop = Event()
def timer(win):
    time_allowance = 300  #seconds
    min_left = time_allowance // 60
    sec_left = time_allowance % 60
    while min_left + sec_left != 0:
        if timer_stop.is_set():
            times_up.set()
            break
        timer = "0" + str(min_left) + ":" + str(sec_left)
        win.addstr(1,1, timer)
        win.refresh()
        time.sleep(1)
        time_allowance -= 1
        min_left = time_allowance // 60
        sec_left = time_allowance % 60
    times_up.set()
  
  
class Errors:
    REPETITION = "tekrarlama"
    NON_LETTER = "harf giriniz"
    MULTIPLE = "tek bir harf giriniz"
        
def hangman(stdscr):
    curses.curs_set(0)
    rows, columns = stdscr.getmaxyx()
    stdscr.clear()
    
    table = Table()
    header = "adam asmaca    çıkmak için: (q)"
    add_centered_str(0, header, stdscr)
    
    footer = "   "
    stdscr.addstr(rows -1, 0, footer)
    
    score = 0
    score_label = "score: "
    def update_score():
        stdscr.addstr(rows - 2, 0, score_label + str(score))
        stdscr.refresh()
    update_score()

    main_win = curses.newwin(10, 80, 3, 2)
    error_win = curses.newwin(3, 80, rows - 2, 15)
    
    timer_win = curses.newwin(3,7, 0, columns - 8)
    timing = Thread(target=timer, args=(timer_win,))
    timing.start()
    
    while not times_up.is_set():
        main_win.clear()
        error_win.clear()
        main_win.refresh()
        error_win.refresh()
        wrong = 0
        main_win.addstr(0,1,Man.pole)
        word = generate_random_word()
        table.generate_board(word)
        main_win.addstr(3,10,table.board)
        main_win.refresh()
        guesses = []
        
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_RED)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_GREEN)

        RED  = curses.color_pair(1)
        GREEN = curses.color_pair(2)

        
        while score == 0 or "_" in table.board:
            if times_up.is_set():
                break
            guess = main_win.getstr().decode(encoding="utf-8").lower()
            if guess == "q":
                timer_stop.set()
                break
            elif guess.upper() in guesses:
                error_win.clear()
                error_win.addstr(0,0, Errors.REPETITION, RED)
                error_win.refresh()
                continue
            elif len(guess) > 1:
                error_win.clear()
                error_win.addstr(0,0, Errors.MULTIPLE, RED)
                error_win.refresh()
                continue
            elif not guess.isalpha():
                error_win.clear()
                error_win.addstr(0,0, Errors.NON_LETTER, RED)
                error_win.refresh()
                continue
            elif guess not in word:
                wrong += 1
                main_win.addstr(0,1,Man.all_forms[wrong])
                if guess == "i":
                    guesses.append("İ")
                else:
                    guesses.append(guess.upper())
                for i, letter in enumerate(guesses):
                    main_win.addstr(4, 9 + 2 * i, letter)
                    
            table.refresh_board(guess, word)
            main_win.addstr(3,10,table.board)
            main_win.refresh()
            if wrong == 6:
                label = "hadi hızlı bir sonrakine!"
                main_win.addstr(5, 10, label)
                main_win.addstr(5, 11 + len(label), "cevap: "+ word, GREEN)
                main_win.addstr(6, 10,  "devam için herhangi bir tuşa bas")
                main_win.refresh()
                stdscr.getch()
                break
                
            error_win.clear()
            error_win.refresh()
            
        if guess == "q":
            break
        elif wrong < 6 and not times_up.is_set():
            score += 1
            update_score()
            
    if times_up.is_set():
        main_win.clear()
        add_centered_str(7, f"süre doldu", main_win)
        add_centered_str(8 , f"skor: {score}", main_win)
        error_win.clear()
        main_win.refresh()
        error_win.refresh()
        stdscr.refresh()
        stdscr.getch()



if __name__ == '__main__':
    wrapper(hangman)
