import os
import sys
import pygame

os.environ['SDL_VIDEODRIVER'] = 'dummy'
pygame.init()

def get_media(file):
    media_info = []
    try:
        with open(file, 'r') as file:
            for line in file:
                if line.startswith("//"):
                    continue
                title, artist, media_path = line.strip().split("|")
                media_info.append({"title": title, "artist": artist, "path": media_path})
    except FileNotFoundError:
        media_info = []
    return media_info
        

def play_songs(playlist_path):
    if ".txt" in playlist_path:
        media_info = get_media(playlist_path)
        playlist = [os.path.abspath(media["path"]) for media in media_info]
    else:
        playlist = [os.path.abspath(playlist_path)]
    while True:
        for song in playlist:
            pygame.mixer.music.load(song)
            pygame.mixer.music.play()
            pygame.time.delay(100)
            while pygame.mixer.music.get_busy():
                pygame.time.Clock().tick(10)
                
            
pl_path = sys.argv
pl_path.pop(0)
play_songs(pl_path[0])

