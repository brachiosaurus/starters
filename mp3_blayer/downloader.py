import os
from pytube import YouTube
from moviepy.editor import VideoFileClip

def download_convert(youtube_url, output_folder=".archive/songs"):
    """download a youtube video and convert it to mp3
    :param youtube_ırl: youtube url of the video to be converted
    :param output_folder: the folder that the outputed mp3 will be stored in, if folder does not exist, it will be created
    :return: path to the mp3 file
    """
    os.makedirs(output_folder, exist_ok=True)

    yt = YouTube(youtube_url)
    video_title = yt.title.replace(" ", "")

    video_file_name = f"{video_title}.mp4"
    mp3_file_path = os.path.join(output_folder, f"{video_title}.mp3")

    yt.streams.get_highest_resolution().download(output_path=output_folder, filename=video_file_name)

    video_clip = VideoFileClip(os.path.join(output_folder, video_file_name))
    video_clip.audio.write_audiofile(mp3_file_path)
    video_clip.close()
    
    os.remove(os.path.join(output_folder, video_file_name))
    return mp3_file_path

if __name__ == "__main__":
    youtube_url = input("Enter the YouTube link: ")
    download_convert(youtube_url)


