import os
import sys
import datetime
import subprocess

from argparse import ArgumentParser, Namespace

from downloader import download_convert

class Files:
    ARCHIVE = "./.archive"
    SONGS = os.path.join(ARCHIVE, "songs")
    ALL_MEDIA = os.path.join(ARCHIVE, "name_path.txt")
    PID_FILE = os.path.join(ARCHIVE, "pid.txt")

def generate_archive():
    os.makedirs(Files.SONGS, exist_ok=True)
    if not os.path.isfile(Files.ALL_MEDIA):
        with open(Files.ALL_MEDIA, 'w') as file:
            file.write("//this file contains names and paths of all media--\n")

def get_media_info(file=Files.ALL_MEDIA) -> list:
    media_info = []
    try:
        with open(file, 'r') as all_med:
            for line in all_med.readlines():
                if line.startswith("//"):
                    continue
                title, artist, media_path = line.split("|")
                media_path = media_path.replace("\n", "")
                media_info.append({"title": title, "artist": artist, "path": media_path})
    except FileNotFoundError:
        media_info = []
    return media_info
    

def start_audio(playlist):
    if not os.path.isfile(Files.PID_FILE):
        audio_player_process = subprocess.Popen(["python3", "player.py", playlist])
        with open(Files.PID_FILE, 'w') as file:
            file.write(str(audio_player_process.pid))
    else:
        with open(Files.PID_FILE, 'r+') as file:
            try:
                audio_player_pid = int(file.read())
                if audio_player_pid:
                    os.kill(audio_player_pid, 9)
            except ValueError:
                audio_player_pid = None
            except ProcessLookupError:
                audio_player_pid = None
            audio_player_process = subprocess.Popen(["python3", "player.py", playlist])
            file.seek(0)
            file.write(str(audio_player_process.pid))

def stop_audio():
    if os.path.isfile(Files.PID_FILE):
        with open(Files.PID_FILE, 'r') as file:
            try:
                audio_player_pid = int(file.read())
                os.kill(audio_player_pid, 9)
            except ValueError:
                print("no media to stop")
    else:
        print("no media to stop")
            
if __name__ == '__main__':
    generate_archive()
    parser = ArgumentParser(description="brachioplayer")
    
    parser.add_argument("-url", "--video-url", nargs=3, help="Video URL, media name, and artist name. mp3 file is stored in ./archive/songs/")
    parser.add_argument("-ls","--list-all", help="display all stored media", action="store_true")
    parser.add_argument("-p","--play", nargs=1, help="song name/all/a. play a song by name or 'all' or 'a' for playing all songs")
    parser.add_argument("-s","--stop", help="stop audio", action="store_true")

    args : Namespace = parser.parse_args()
    
    if args.video_url:
        video_url, title, artist = args.video_url
        media_info = get_media_info()
        media_names = [media["title"] for media in media_info]
        if title in media_names:
            choice = input(f"{title} by is a duplicate. do you want to override(y/[n])?: ")
            if choice.lower() != "y":
                sys.exit("Execution stopped as per user's choice.")
        media_path = download_convert(video_url)
        with open("./.archive/name_path.txt", 'a') as file:
            file.write(f"{title}|{artist}|{media_path}\n")
    if args.list_all:
        media_info = get_media_info()
        if media_info:
            print("--all media-- ")
            for number, media in enumerate(media_info, start=1):
                print(f"{number}- {media['title']} by {media['artist']}")
        else:
            print("nothing to show\nuse -url [YOUTUBE_URL] [TITLE] [CREATOR] to download from youtube")
    if args.play:
        choice = args.play
        playlist = ""
        if "all" in choice or "a" in choice:
            playlist = Files.ALL_MEDIA
        else:
            song = choice[0]
            media_info = get_media_info()
            for media in media_info:
                if media["title"] == song:
                    playlist = media["path"]
                    break
            if not playlist:
                sys.exit(f"err: {song} is not among downloaded songs")
            
        start_audio(playlist)
        
    if args.stop:
        stop_audio()
                
        

    

