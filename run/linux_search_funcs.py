import os
import subprocess


class FileList(list):
    def sort_by_access_time(self):
        def get_access_time(file_path):
            try:
                return os.stat(file_path).st_atime
            except FileNotFoundError:
                return 1
        self.sort(key=get_access_time, reverse=True)  # smaller the number, earlier in time

    def filter_by_keyword(self, keyword):
        self[:] = [path for path in self if keyword in os.path.basename(path)]
        return self

    def find_match_inside_files(self, keyword, extension="", line_start="") -> list:
        matches = []

        for file_path in self:
            if extension in file_path:
                if keyword_in_file(file_path, keyword, line_start):
                    matches.append(file_path)
        if matches:
            return matches[0]


def keyword_in_file(file_path, keyword, line_start=""):
    try:
        with open(file_path, 'r') as f:
            for line in f:
                if line.startswith(line_start):
                    line_content = line.strip().replace(line_start, "")
                    return keyword.lower() in line_content.lower()
    except Exception as e:
        print(f"an error occurred while reading {file_path}: {e}")
        return False


common_extensions = [
        '.txt', '.pdf', '.doc', '.docx', '.xls', '.xlsx', '',  # Document formats
        '.png', '.jpg', '.jpeg', '.gif', '.bmp', '.svg',  # Image formats
        '.mp3', '.wav', '.ogg', '.flac', '.aac',  # Audio formats
        '.mp4', '.avi', '.mkv', '.mov', '.wmv',  # Video formats
        '.py', '.c', '.cpp', '.java', '.html', '.css', '.js',  # Code and web formats
        '.desktop', '.sh'
    ]


def filter_directory(directory, extensions: list, home=False):  # filtering first-hand reduces runtime significantly
    """
    :param directory: to list the files of
    :param extensions: file extension(s) wanted to be included in filter
    :param home: exclude installation directories if true
    :return:
    """
    filtered_files = []
    if home:
        for root, dirs, files in os.walk(directory):
            dirs_to_exclude = [d for d in dirs if
                               d.startswith('.') or 'bin' in os.listdir(os.path.join(root, d)) or 'lib' in os.listdir(
                                   os.path.join(root, d))]
            for d in dirs_to_exclude:
                dirs.remove(d)  # Exclude hidden directories and those containing "bin" or "lib"

            for file_name in files:
                if os.path.splitext(file_name)[1].lower() in extensions and not file_name.startswith("."):
                    filtered_files.append(os.path.join(root, file_name))

        return FileList(filtered_files)

    for root, dirs, files in os.walk(directory, onerror=None):
        dirs_to_exclude = [d for d in dirs if d.startswith('.')]
        for d in dirs_to_exclude:
            dirs.remove(d)
        for file_name in files:
            if os.path.splitext(file_name)[1].lower() in extensions and not file_name.startswith("."):
                filtered_files.append(os.path.join(root, file_name))

    return FileList(filtered_files)


def open_desktop_application(desktop_file_path):
    try:
        with open(desktop_file_path, 'r') as f:
            for line in f:
                if line.startswith("Exec="):
                    command = line.strip().replace("Exec=", "")
                    subprocess.run(command, shell=True)
                    break
    except Exception as e:
        print(f"An error occurred: {e}")


def search_best_result(keyword: str):
    """
    scan the files with common extensions in the home folder if no match, applications folder still nothing, root
    :param keyword: search for this
    :return: path of the last accessed file that's basename contains the keyword
    :raise: NoMatchesError if the return value is None
    """
    usr_dir = "/usr/"
    user_home = os.path.expanduser("~")
    applications = '/usr/share/applications/'

    # search among home directory, filter out home
    home_content = filter_directory(user_home, common_extensions, home=True)
    matches = home_content.find_match_inside_files(keyword, ".desktop", "Name=")
    home_content.filter_by_keyword(keyword)
    if matches:
        return matches
    elif home_content:
        home_content.sort_by_access_time()
        return home_content[0]

    # search among .desktop files stored at applications
    extension_desktop = [".desktop"]
    applications_content = filter_directory(applications, extension_desktop)
    applications_content.sort_by_access_time()
    matches = applications_content.find_match_inside_files(keyword, ".desktop", "Name=")
    if matches:
        return matches

    # if not at home
    extension_sh = [".sh"]
    home_again_sh = filter_directory(user_home, extension_sh)
    home_again_sh.filter_by_keyword(keyword)
    if home_again_sh:
        home_again_sh.sort_by_access_time()
        return home_again_sh[0]

    # better not search for something not found there...
    print("hard to find what you're looking for !!")
    if input("continue search? (return/n)").lower() == "n":
        raise Exception
    usr_content = filter_directory(usr_dir, common_extensions)
    usr_content.filter_by_keyword(keyword)
    if usr_content:
        usr_content.sort_by_access_time()
        return usr_content[0]

    raise Exception


def run_the_match(filepath: str):
    basename = os.path.basename(filepath)
    if ".desktop" in basename:
        open_desktop_application(f'{filepath}')
    elif ".sh" in basename:
        os.system(f'{filepath}')
    else:
        os.system(f"open '{filepath}'")


if __name__ == '__main__':
    # open_desktop_application("")
    list_ = search_best_result(input("kw: "))
    print(list_)
    run_the_match(list_)
