import os
import time
import subprocess

username = os.getlogin()
directories = {
    "programs": 'C:\\Program Files',
    "desktop": f'C:\\Users\\{username}\\Desktop',
    "user": f'C:\\Users\\{username}'
}

def search_key_in(keyword, directory):
    try:
        file_basename = get_extension(keyword.lower(), directory)
        if not file_basename:
            return None

        command = ['where', '/r', f'{directory}', file_basename]
        string_command = f"where /r {directory} {file_basename}"
        print(f"writing command: {string_command} to cmd")
        result = subprocess.run(command, capture_output=True, text=True, check=True)
        output = result.stdout.strip()

        if output:
            matches = output.splitlines()
            matchh = matches[0]
            print("successfully returned the match!", matchh)
            return matchh
        else:
            return None
    except subprocess.CalledProcessError:
        print("this exc is caused by the subprocess", subprocess.CalledProcessError)
    except Exception as e:
        print("an exception occurred while search_in_key:", e)


def get_extension(keyword, directory):
    def walk_error(exception_instance):
        print("error occurred during os.walk")

    mathcing_files = []
    for root, dirs, files in os.walk(directory, onerror=walk_error):
        for file_name in files:
            if keyword in file_name.lower() and not file_name.startswith("~"):
                mathcing_files.append(file_name)

    def get_access_time(file_path):
        try:
            return os.stat(file_path).st_atime
        except FileNotFoundError:
            return 1

    if mathcing_files:
        try:
            mathcing_files.sort(key=get_access_time, reverse=True)  # smaller the number, earlier in time
            matchhh = mathcing_files[0]
            print("get_extension succesfully returns:", matchhh)
            return matchhh
        except Exception as e:
            print("exception occurred during sort/return of get_extension :", e)
    print("get_extension returns False")
    return False


def search_key(keyword):
    matching = search_key_in(keyword, directories["programs"])
    if matching:
        return matching
    print("not in program files")
    matching = search_key_in(keyword, directories["desktop"])
    if matching:
        return matching
    print("not at desktop")
    matching = search_key_in(keyword, directories["user"])
    if matching:
        return matching

    raise Exception("this exc occurred because nothing was returned from search_key")


def open_match(filepath):
    try:
        os.system(f'start "" "{filepath}"')
    except Exception as e:
        print("this exc occurred while opening the match:", e)



if __name__ == '__main__':

    work = True
    print("  enter the name of a file or an app and i will open it for you !!")
    while work:
        input_prompt = "\n ⦿ (enter qqq to quit) run: "
        searched_keyword = input(input_prompt)
        if searched_keyword.lower() == "qqq":
            work = False
            break
        try:
            best_result = search_key(searched_keyword)
            print(f"openning {os.path.basename(best_result)}...")
            open_match(best_result)
            time.sleep(2)
        except Exception as e:
            print(e)
            print("no matches were found :/")
