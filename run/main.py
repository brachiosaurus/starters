import os
import time
from sys import platform

import linux_search_funcs
import darwin_funcs
import win_funcs


if __name__ == '__main__':
    work = True
    print("  enter the name of a file or an app and i will open it for you !!")
    input_prompt = "\n ⦿ (enter qqq to quit) run: "
    while work:
        if platform == "linux" or platform == "linux2":
            input_prompt = "\n\033[1;32m ⦿\033[0m (enter qqq to quit) \033[1;34mrun: \033[0m"
            searched_keyword = input(input_prompt)
            if searched_keyword.lower() == "qqq":
                work = False
                break
            try:
                best_result = linux_search_funcs.search_best_result(searched_keyword)
                print(f"openning {os.path.basename(best_result)}...")
                linux_search_funcs.run_the_match(best_result)
                time.sleep(2)
            except Exception:
                print("no matches were found :/")

        elif platform == "darwin":
            searched_keyword = input(input_prompt)
            if searched_keyword.lower() == "qqq":
                work = False
                break
            try:
                best_result = darwin_funcs.search_key(searched_keyword)
                print(f"openning {os.path.basename(best_result)}...")
                darwin_funcs.open_match(best_result)
                time.sleep(2)
            except Exception:
                print("no matches were found :/")

        elif platform == "win32":
            searched_keyword = input(input_prompt)
            if searched_keyword.lower() == "qqq":
                work = False
                break
            try:
                best_result = win_funcs.search_key(searched_keyword)
                print(f"openning {os.path.basename(best_result)}...")
                win_funcs.open_match(best_result)
                time.sleep(2)
            except Exception:
                print("no matches were found :/")
