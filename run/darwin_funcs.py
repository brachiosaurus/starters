import os
import subprocess



def search_key_in(keyword, apps=False):  # i feel like cheating when using spotlight :d
    try:
        command = ['mdfind', keyword]
        if apps:
            command = ['mdfind', '-onlyin', '/Applications', keyword]
        result = subprocess.run(command, capture_output=True, text=True, check=True)
        output = result.stdout.strip()
        if output:
            matches = output.splitlines()
            return matches[0]
        else:
            return None
    except subprocess.CalledProcessError:
        return None


def search_key(keyword):
    matching = search_key_in(keyword, apps=True)
    if matching:
        return matching

    matching = search_key_in(keyword)
    if matching:
        return matching

    raise Exception


def open_match(filepath):
    os.system(f"open '{filepath}'")


if __name__ == '__main__':
    try:
        match = search_key(input("kw: "))
    except Exception:
        print("no matches")
