import math

import numpy as np
import sounddevice as sd


def capture_audio(duration, sample_rate=44100):
    total_samples = duration * sample_rate

    print("recording...")
    audio_data = sd.rec(total_samples, samplerate=sample_rate,
                        channels=1)
    sd.wait()
    print("recording complete !!\n")

    return audio_data


def apply_window(audio):
    window = np.hamming(len(audio))
    return audio * window


def get_dominant_frequency(audio, sample_rate=44100):

    # fast fourier transform
    fft_result = np.fft.fft(audio)
    fft_magnitude_rough = np.abs(fft_result)
    fft_magnitude = np.convolve(fft_magnitude_rough.flatten(), np.ones(3) / 3, mode='same')
    num_samples = len(audio)
    frequency_resolution = sample_rate / num_samples

    all_frequencies = np.arange(0, num_samples) * frequency_resolution
    dominant_frequency_index = np.argmax(fft_magnitude)
    wanted_frequency = all_frequencies[dominant_frequency_index]
    return wanted_frequency


def find_note(frequency):
    global notes_current
    octaves = {
        (16.35, 32.7): 0,
        (32.8, 65.4): 1,
        (65.5, 130.8): 2,
        (130.9, 261.6): 3,
        (261.7, 523.3): 4,
        (523.4, 1046.5): 5,
        (1046.6, 2093.0): 6,
        (2093.1, 4186.0): 7,
        (4186.1, 8372.0): 8,
        (8372.1, 16744.0): 9,
        (16744.1, 33488.0): 10,
        (33488.1, 9999999): 11
    }
    octave = None
    note = None

    # identify the frequency
    for frequency_range, octave_value in octaves.items():
        lower_bound, upper_bound = frequency_range
        if lower_bound <= frequency < upper_bound:
            octave = octave_value
            octave_range = upper_bound - lower_bound
            semitone_size = octave_range / 12
            main_note_index = math.floor((frequency - lower_bound) / semitone_size)
            note = notes_current[main_note_index]
            break
    identification = f"{note} {octave}"
    if note and octave:
        return identification
    else:
        return "silence?"
    # print(note, end=" ")
    # print(octave)


def settings(setting):
    global notation
    global symbol

    if setting.lower() == "n":
        if notation == "english":
            notation = "solfeggio"
        elif notation == "solfeggio":
            notation = "english"
    elif setting.lower() == "s":
        if symbol == "flat":
            symbol = "sharp"
        elif symbol == "sharp":
            symbol = "flat"
    else:
        pass


def first_options():
    global work
    global notes_current
    while work:
        option = input("hit return to start\ni for info  s for settings q to quit")
        if option.lower() == "i":
            print(f"--info--\nstandard: A4 440Hz\nnotation: {notation}\nsymbol: {symbol}\n")
        elif option.lower() == "s":
            settings(input(f"--settings--\nnotation: {notation}, symbol: {symbol}\ntoggle notation with n, symbol with s"))
            if notation == "english" and symbol == "flat":
                notes_current = notes_e_f
            elif notation == "english" and symbol == "sharp":
                notes_current = notes_e_s
            elif notation == "solfeggio" and symbol == "flat":
                notes_current = notes_s_f
            elif notation == "solfeggio" and symbol == "sharp":
                notes_current = notes_s_s
            print("\n")
        elif option.lower() == "q":
            work = False
        else:
            break


if __name__ == '__main__':

    work = True
    
    notation = "english"
    symbol = "flat"

    notes_e_f = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]
    notes_e_s = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    notes_s_f = ["Do", "Reb", "Re", "Mib", "Mi", "Fa", "Solb", "Sol", "Lab", "La", "Sib", "Si"]
    notes_s_s = ["Do", "Do#", "Re", "Re#", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "La#", "Si"]
    notes_current = notes_e_f

    print("note identifier !!")

    first_options()

    while work:
        captured_audio = capture_audio(3)  # seconds
        windowed_audio = apply_window(captured_audio[:, 0])
        dominant_frequency = get_dominant_frequency(windowed_audio)
        # print(dominant_frequency)

        result = find_note(dominant_frequency)
        print(result)

        is_quit = input("\nhit return to continue\nh for homepage q to quit")
        if is_quit.lower() == "q":
            work = False
        if is_quit.lower() == "h":
            first_options()
