import socket
import sys



client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('127.0.0.1', 8888)
client_socket.connect(server_address)

greeting = """
---welcome to my restaurant---
please type
'm' to see the menu
the number of the food you desire and the portion count
(1 2 adds 2 portions of food no 1 to you order)
'e' to edit your order
's' to submit you order
'q' to quit
"""


menu = {
"lentil soup": 50,
"green salad": 70,
"adana kebab": 200,
"water": 10,
}

order = []
cost = 0
    
print(greeting)
try:
    while True:
        

        user_input = input("-> ")
        
        if user_input.lower() == 'q':
            client_socket.send("canceled".encode())
            break
        elif user_input.lower() == 'm':
            print("---menu---")
            for no, (item, price) in enumerate(menu.items(), start=1):
                print(no, ")", item, price, "₺")
        elif user_input.lower() == 'e':
            order_to_show = '\n'.join([str(i + 1) + "-" + item for i, item in enumerate(order)])
            message = f"--your order---\n{order_to_show}\ncost: {cost}"
            print(message)
            item_i_to_delete = input("enter the number of the line you want to remove(0 to quit): ")
            if item_i_to_delete == "0":
                continue
            elif not item_i_to_delete.isdigit():
                print("please enter a number", file=sys.stderr)
                continue
            elif int(item_i_to_delete) > len(order):
                print("invalid choice", file=sys.stderr)
                continue
            item_i_to_delete = int(item_i_to_delete) - 1
            line = order[item_i_to_delete].split()
            line.pop(len(line) - 1)
            line.pop(len(line) - 1)
            cost -= menu[" ".join(line)] * portions
            order.pop(item_i_to_delete)
        elif user_input.lower() == 's':
            order_to_show = '\n'.join(order)
            submit_message = f"--your order---\n{order_to_show}\ncost: {cost}"
            print(submit_message)
            if input("do you want to continue?[y]/n: ").lower() == "n":
                continue
            else:
                order_to_send = "\n".join(["* " + item for item in order])
                print(order)
                if cost == 0:
                    order_to_send = "canceled"
                client_socket.send(order_to_send.encode())
                print("we got your order!")
                break
        elif user_input.replace(" ", "").isdigit():
            args = user_input.split()
            
            if len(args) != 2 :
                print("please enter two numbers separated with a space", file=sys.stderr)
                continue
                
            args[0] = int(args[0]) - 1
            args[1] = int(args[1])
            
            foods = list(menu.keys())
            
            if args[0] > len(foods):
                print("invalid item number", file=sys.stderr)
                continue
            
            selected_food = foods[args[0]]
            portions = args[1]
            order.append(selected_food + " x " + str(portions))
            cost += (menu[selected_food] * portions)
        else:
            print("invalid input", file=sys.stderr)
except ConnectionError:
    print("Connection lost :/ please retry", file=sys.stderr)
        
        
client_socket.close()

