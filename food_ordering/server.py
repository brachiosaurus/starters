import socket
from datetime import datetime

BUFFER = 1024
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('127.0.0.1', 8888)
server_socket.bind(server_address)

server_socket.listen()

print("---welcome back---")
now = datetime.now()
now_formatted = now.strftime("%d %b %Y %H:%M")
print(f"server start: {now_formatted}\n use ^C to stop")


clients = {}
next_client_number = 1

while True:
    try:
        client_socket, client_address = server_socket.accept()

        client_number = next_client_number
        next_client_number += 1
        
        clients[client_number] = client_socket
        
        
        while True:
            try:
                data = client_socket.recv(BUFFER).decode().strip()
                
                if not data:
                    break
                elif data == "canceled":
                    next_client_number -= 1
                    break
                

                print(f"\nOrder {client_number}:\n-------\n{data}")
            
            except ConnectionResetError:
                break
        
        client_socket.close()
        del clients[client_number]
    except KeyboardInterrupt:
        now = datetime.now()
        now_formatted = now.strftime("%d %b %Y %H:%M")
        print(f"goodbye. \nserver stops at: {now_formatted}")
        break


server_socket.close()
