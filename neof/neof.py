import platform
import os
import subprocess
import psutil
import time, datetime
import sys

from argparse import ArgumentParser, Namespace

endcolor = "\033[0;0m"
class Colors:
    red = '\033[1;31m'
    green = '\033[1;32m'
    yellow = '\033[1;33m'
    blue = '\033[1;34m'
    purple = '\033[1;35m'
    cyan = '\033[1;36m'
    white = '\033[1;37m'

class Logos:
    linux =[
"              a8888b.       ",
"             d888888b.      ",
"             8P^YP^Y88      ",
"             8|o||o|88      ",
"             8'    .88      ",
"             8`._.' Y8.     ",
"            d/      `8b.    ",
"           dP   .    Y8b.   ",
"          d8:'  '  `::88b   ",
"         d8'         'Y88b  ",
"        :8P    '      :888  ",
"         8a.   :     _a88P  ",
"       ._/'Yaa_:   .| 88P|  ",
"       \    Y2P    `| 8P  `.",
"       /     \.___.d|    .' ",
"       `--..__)8888P`._.'   "]

    apple = [
"                   ,xNMM.     ",
"               .OMMMMo        ",
"               OMMM0,         ",
"     .;loddo:' loolloddol;.   ",
"   cKMMMMMMMMMMNWMMMMMMMMMM0: ",
" .KMMMMMMMMMMMMMMMMMMMMMMMWd. ",
" XMMMMMMMMMMMMMMMMMMMMMMMX.   ",
";MMMMMMMMMMMMMMMMMMMMMMMM:    ",
":MMMMMMMMMMMMMMMMMMMMMMMM:    ",
".MMMMMMMMMMMMMMMMMMMMMMMMX.   ",
" kMMMMMMMMMMMMMMMMMMMMMMMMWd. ",
" .XMMMMMMMMMMMMMMMMMMMMMMMMMMk",
"  .XMMMMMMMMMMMMMMMMMMMMMMMMK.",
"    kMMMMMMMMMMMMMMMMMMMMMMd  ",
"     ;KMMMMMMMWXXWMMMMMMMk.   ",
"       .cooc,.    .,coo:.     "]

    windows = [
"        ,.=:!!t3Z3z.,               ",
"       :tt:::tt333EE3               ",
"       Et:::ztt33EEEL @Ee.,      ..,",
"      ;tt:::tt333EE7 ;EEEEEEttttt33#",
"     :Et:::zt333EEQ. $EEEEEttttt33QL",
"     it::::tt333EEF @EEEEEEttttt33F ",
"    ;3=*^```'*4EEV :EEEEEEttttt33@. ",
"    ,.=::::!t=., ` @EEEEEEtttz33QF  ",
"   ;::::::::zt33)   '4EEEtttji3P*   ",
"  :t::::::::tt33.:Z3z..  `` ,..g.   ",
"  i::::::::zt33F AEEEtttt::::ztF    ",
" ;:::::::::t33V ;EEEttttt::::t3     ",
" E::::::::zt33L @EEEtttt::::z3F     ",
"{3=*^```'*4E3) ;EEEtttt:::::tZ`     ",
"             ` :EEEEtttt::::z7      ",
"                 'VEzjt:;;z>*`      "
]

to_be_printed = [] 

def info(key, value):
    text = f"{color + key + endcolor}: {value}"
    to_be_printed.append(text) 

def title(title):
    text = color + title + endcolor 
    underline = "-"*len(title)
    to_be_printed.append(text) 
    to_be_printed.append(underline) 

TITLE = os.getlogin() + "@" + platform.node()
HOST = platform.node()
KERNEL = platform.release()

uptime_messy = subprocess.check_output(['uptime']).decode()
UPTIME = uptime_messy.split('up', 1)[1].split(',')[0].strip()

SHELL = os.path.basename(os.environ["SHELL"])
CPU = platform.processor()

OS = "Unknown"
logo = None
if sys.platform == 'linux' or sys.platform == 'linux2':
    logo = Logos.linux
    OS = platform.freedesktop_os_release()["PRETTY_NAME"]
elif sys.platform == 'darwin':
    logo = Logos.apple
    OS = "MacOS"
elif sys.platform == 'windows':
    logo = Logos.windows
    OS = "Windows"

def print_the_result():
    if logo is None:
        for info in to_be_printed:
            print(info)
    else:
        for i, text in enumerate(to_be_printed):
            logo[i] += "   " + text


        for line in logo:
            print(line)


if __name__ == '__main__':

    parser = ArgumentParser()
    
    parser.add_argument("--off", help="hides the logo", action="store_true")
    parser.add_argument("--ascii_os", help="choose acsii art", choices=["macos", "windows", "linux"])
    parser.add_argument("-c", "--color", help="choose color", choices=["red", "green", "yellow", "blue", "purple", "cyan"])
    args : Namespace = parser.parse_args()
    
    if args.off:
        logo = None
        
    if args.ascii_os == "macos":
        logo = Logos.apple
    elif args.ascii_os == "windows":
        logo = Logos.windows
    elif args.ascii_os == "linux":
        logo = Logos.linux
    
    color = Colors.green
    
    if args.color == "red":
        color = Colors.red
    elif args.color == "green":
        color = Colors.green
    elif args.color == "yellow":
        color = Colors.yellow
    elif args.color == "blue":
        color = Colors.blue
    elif args.color == "purple":
        color = Colors.purple
    elif args.color == "cyan":
        color = Colors.cyan
    
    
#configuration
#comment out the info you dont want to be shown
    title(TITLE)
    info("OS", OS)
    info("Host", HOST)
    info("Kernel", KERNEL)
    info("Uotime", UPTIME)
    info("Shell", SHELL)
    info("CPU", CPU)

    print_the_result()






