
def format_input(input_expression):
    chars = list(input_expression)
    add_space_at = []
    for i, char in enumerate(chars):  #this might be kinda ineffective but i dont know why
        previous = chars[i - 1]
        if char == " " and (previous == " " or i == 0):
            del chars[i]
        elif char == "x":
            chars[i] = "*"
        elif char == "÷":
            chars[i] = "/"
        if char in "*-+/" and previous != " ":
            add_space_at.append(i)

# if the given expression is invalid, like "aaaaaaaaaa" i get a runtime error here !! (do not handle this with except RuntimeError, it will take and use too much. try and find another way.
    for j in add_space_at:
        chars.insert(j, " ")

    return "".join(chars)
# maybe, do not prioritise th euse of strings and tyr typeasting the inputs elements instead. if you get an error, typeerror most probably, the given expression was invalid therefore it also helps exception handling
# yet the operators cannot also be typecasted... Maybe dont let it get the type error in the first place and solve the problem beforehand. 
# but how?..
def interpret_input(input_expression: str) -> list:
    input_splitted = input_expression.split(' ')

    n_one = None
    n_two = None
    operator_ = None
    while True:
        interpretation = []
        for i, string in enumerate(input_splitted):

            if string not in "*-+/" and n_one is None:
                n_one = i
            elif string not in "*-+/" and n_two is None:
                n_two = i
            elif string not in "*-+/" and n_one is not None and n_two is not None:
                interpretation.append(input_splitted[n_one])
                n_one += 1
                n_two += 1
            elif string in "*-+/" and operator_ is None:
                operator_ = i

            if n_one is not None and n_two is not None and operator_ is not None:
                sub_operation = input_splitted[n_one] + input_splitted[operator_] + input_splitted[n_two]
                sub_result = str(eval(sub_operation))
                interpretation.append(sub_result)
                n_one = None
                n_two = None
                operator_ = None
            elif operator_ is not None and n_two is None:
                if n_one is not None:
                    interpretation.append(input_splitted[n_one])
                    n_one = None
                interpretation.append(input_splitted[operator_])
                operator_ = None

        input_splitted[:] = interpretation
        if len(input_splitted) == 1:
            return input_splitted



if __name__ == '__main__':
    print("enter operation in polish notation, h for help q to quit.")
    while True:
        input_expression = input("operation : ")
        if input_expression == "h":
            print("""
    Reverse Polish Notation
In reverse Polish notation, the operators follow their operands. 
For example, to add 3 and 4 together, the expression is 3 4 + rather than 3 + 4. 
The expression 3 − 4 + 5 in conventional notation is 3 4 − 5 + in reverse Polish notation: 4 is first subtracted from 3, 
then 5 is added to it.
            """)
        elif input_expression == "q":
            break
        else:
            input_expression = format_input(input_expression)
            result = interpret_input(input_expression)
            print(result[0])

