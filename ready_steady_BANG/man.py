class Man:
    left_skins = {"start": "( ._.)       ",
                  "aiming": "( -_•)╦̵╤─   ",
                  "shooting1": "( -_-)╦╤─   -",
                  "shooting2": "( •_•)╦̵╤─   ",
                  "missed": "( ⚆ _ ⚆ )\"   ",
                  "shot": "(´×_×`)..    ",
                  "won": "(ﾉ^ヮ^)ﾉ\"     "}

    right_skins = {"start": "       (._. )",
                   "aiming": "   ─╦̵╤(•_- )",
                   "shooting1": "-   ─╦╤(-_- )",
                   "shooting2": "   ─╦̵╤(•_• )",
                   "missed": "      џ(ºДºџ)",
                   "shot": "      ..(x⸑x)",
                   "won": "   ( •̀ ᴗ •́ )/"}

    def __init__(self, direction):
        self.direction = direction
        self.score = 0
        self.skins = {}
        if direction == "right":
            self.skins = self.right_skins
        elif direction == "left":
            self.skins = self.left_skins

    def show(self, skin, columns=0) -> list:
        if self.direction == "left":
            args = [1, 0, self.skins[skin]]
        else:
            args = [1, columns - len(self.skins[skin]), self.skins[skin]]
        return args