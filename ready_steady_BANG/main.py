import time
import curses

from curses import wrapper
from threading import Thread, Event
from random import randint

from man import Man


def horizontal_padding(obj_width: int, stdscr):
    rows, columns = stdscr.getmaxyx()
    return (columns - obj_width) // 2


def screen(stdscr):

    curses.curs_set(0)
    rows, columns = stdscr.getmaxyx()

    if columns < 60 or rows < 15:
        stdscr.clear()
        stdscr.addstr((rows - 1)//2, (columns - 34)//2, "please enlarge the terminal (60x15)")
        stdscr.refresh()
        time.sleep(1)
        quit()

    stdscr.clear()

    # header and footer
    stdscr.addstr(0, horizontal_padding(13, stdscr), "pistol duel !", curses.A_ITALIC)
    stdscr.addstr(3, 0, "(a)", curses.A_DIM)
    stdscr.addstr(3, columns - 3, "(i)", curses.A_DIM)
    stdscr.addstr(rows - 2, horizontal_padding(6, stdscr), "scores", curses.A_UNDERLINE)
    stdscr.addstr(rows - 1, 0, "press 'q' to quit", curses.A_DIM)
    stdscr.refresh()

    # main window
    main_window = curses.newwin(rows - 6, columns, 4, 0)
    main_window.clear()

    man_left = Man("left")
    main_window.addstr(*man_left.show("start"))

    man_right = Man("right")
    main_window.addstr(*man_right.show("start", columns))

    def show_prompt(prompt, columns):
        return [0, (columns - len(prompt))//2, prompt, curses.A_REVERSE]

    prompt = "hit a key to start"
    main_window.addstr(*show_prompt(prompt, columns))
    main_window.refresh()

    # score window
    score_window = curses.newwin(1, 5, rows - 1, (columns - 3) // 2)
    score_window.clear()
    score_window.addstr(f"{man_left.score}|{man_right.score}")
    score_window.refresh()

    stdscr.refresh()
    key = stdscr.getkey()
    if key == "q":
        return None

    # now lets get started
    def main():
        main_window.clear()
        main_window.addstr(*man_left.show("aiming"))
        main_window.addstr(*man_right.show("aiming", columns))

        ingame_prompt = ""

        def timer():
            global ingame_prompt
            ingame_prompt = " ready "
            main_window.addstr(*show_prompt(ingame_prompt, columns))
            main_window.refresh()
            time.sleep(randint(1,3)/2)
            ingame_prompt = "steady"
            main_window.addstr(*show_prompt(ingame_prompt, columns))
            main_window.refresh()
            time.sleep(randint(1, 10)/2)
            ingame_prompt = " BANG! "
            main_window.addstr(*show_prompt(ingame_prompt, columns))
            main_window.refresh()
            event.set()

        event = Event()
        timing = Thread(target=timer)
        timing.start()

        left_missed = False
        right_missed = False
        draw = False

        while True:
            key = stdscr.getkey()
            if event.is_set():
                break
            else:
                if key == "a":
                    main_window.addstr(*man_left.show("missed"))
                    left_missed = True
                    main_window.refresh()

                elif key == "i":
                    main_window.addstr(*man_right.show("missed", columns))
                    right_missed = True
                    main_window.refresh()

                elif key == "q":
                    stdscr.addstr(rows - 1, 0, "wait a sec...    ", curses.A_DIM)
                    stdscr.refresh()
                    timing.join()
                    quit()

                if left_missed and right_missed:
                    timing.join()
                    ingame_prompt = "DRAW!"
                    draw = True
                    main_window.addstr(*show_prompt(ingame_prompt, columns))
                    main_window.refresh()
                    time.sleep(2)
                    ingame_prompt = "press a key to continue"
                    main_window.addstr(*show_prompt(ingame_prompt, columns))
                    main_window.refresh()

        if key == "a" and not draw:
            main_window.clear()
            main_window.addstr(*man_left.show("shooting1"))
            main_window.addstr(*man_right.show("missed", columns))
            main_window.refresh()
            time.sleep(0.5)
            main_window.addstr(*man_left.show("shooting2"))
            main_window.addstr(*man_right.show("shot", columns))
            ingame_prompt = "left wins !"
            man_left.score += 1
            main_window.addstr(*show_prompt(ingame_prompt, columns))
            main_window.refresh()
            time.sleep(1)
            main_window.addstr(*man_left.show("won"), curses.A_BLINK)
            main_window.refresh()
        elif key == "i" and not draw:
            main_window.clear()
            main_window.addstr(*man_left.show("missed"))
            main_window.addstr(*man_right.show("shooting1", columns))
            main_window.refresh()
            time.sleep(0.5)
            main_window.addstr(*man_left.show("shot"))
            main_window.addstr(*man_right.show("shooting2", columns))
            ingame_prompt = "right wins !"
            man_right.score += 1
            main_window.addstr(*show_prompt(ingame_prompt, columns))
            main_window.refresh()
            time.sleep(1)
            main_window.addstr(*man_right.show("won", columns), curses.A_BLINK)
            main_window.refresh()
        elif draw:
            main_window.clear()
            main_window.addstr(*man_left.show("shot"), curses.A_BLINK)
            main_window.addstr(*man_right.show("shot", columns), curses.A_BLINK)
            ingame_prompt = "be more careful next time !"
            main_window.addstr(*show_prompt(ingame_prompt, columns))
            main_window.refresh()

    while True:
        main()
        score_window.clear()
        score_window.addstr(f"{man_left.score}|{man_right.score}")
        score_window.refresh()
        key = stdscr.getkey()
        if key == "q":
            break
        key = stdscr.getkey()
        if key == "q":
            break
        else:
            continue


if __name__ == '__main__':
    wrapper(screen)
